import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { Router } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { ViewDetailPageComponent } from '../view-detail-page/view-detail-page.component';

export interface Employee {
  id: string;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: any;
  basicSalary: any;
  status: string;
  group: string;
  description: any;
}

/** Constants used to fill up our data base. */
const group: string[] = [
  'Frontend Developer',
  'Backend Developer',
  'Quality Assurance',
  'Project Manager',
  'Business Analyst',
  'Sales',
  'Human Resource',
  'Office Boy',
  'Manager'
];
const NAMES: string[] = [
  'Maya',
  'Algibran',
  'Oliv',
  'Agung',
  'Amelia',
  'Jacky',
  'Caca',
  'Tari',
  'Imah',
  'Olivia',
  'Isabel',
  'Jaka',
  'Clara',
  'Viona',
  'Ahmad',
  'Mia',
  'Tora',
  'Eliza',
  'Jessica',
  'Dzaki',
  'Ramlan',
  'Jesaya',
  'Panji',
  'Eko',
  'Pandi',
  'Topan',
  'Ryan'
];
const STATUS: string[] = [
  'Intern',
  'Part-time',
  'Full-time',
  'Contract'
]

const bD: string[] = [
  '31-05-1997',
  '01-02-2000',
  '05-06-2005'
]

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [MatFormFieldModule, MatInputModule, MatTableModule, MatSortModule, MatPaginatorModule, MatButtonModule, MatDialogModule ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'username', 'status', 'group', 'action'];
  dataSource: MatTableDataSource<Employee>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort: MatSort = new MatSort;

  constructor(
    private router: Router,
    public dialog: MatDialog,
  ) {
    // Create 100 users
    const users = Array.from({ length: 100 }, (_, k) => createNewUser(k + 1));
    console.log('users -> ', users);

    // localStorage.setItem('data', JSON.stringify(this.dataSource.data));
    // console.log('home localStorage -> ', localStorage);
    
    // localStorage.removeItem('key');
    // localStorage.clear();


    // x?.concat(this.dataSource.data);

    let x = localStorage.getItem('dataNewEmployee');
    console.log('x', [x]);

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
    // this.dataSource = new MatTableDataSource(x.concat(users));

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onBack() {
    this.router.navigate(['/'])
  }

  addEmployee() {
    this.router.navigate(["/add-employee-page"]);
  }

  addData() {
    // const randomElementIndex = Math.floor(Math.random() * ELEMENT_DATA.length);
    // this.dataSource.push(ELEMENT_DATA[randomElementIndex]);
    // this.table.renderRows();


    // this.dataSource.data = ELEMENT_DATA;
  }

  removeData() {
    // this.dataSource.pop();
    // this.table.renderRows();
  }

  onView(obj: { row: any }) {
    const dialogRef = this.dialog.open(ViewDetailPageComponent, {
      width: "50%",
      data: {
        data: obj
      }
    })
  }
}

/** Builds and returns a new User. */
function createNewUser(id: number): Employee {
  const firstName = NAMES[Math.round(Math.random() * (NAMES.length - 1))]

  const lastName = NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0);

  const status = STATUS[Math.round(Math.random() * (STATUS.length - 1))]

  const birthDate = bD[Math.round(Math.random() * (bD.length - 1))]

  const desc = bD[Math.round(Math.random() * (bD.length - 1))]

  return {
    // status: Math.round(Math.random() * 100).toString(),
    id: id.toString(),
    username: firstName +' '+ lastName,
    firstName: firstName,
    lastName: lastName+'.',
    email: firstName+lastName+'@gmail.com',
    birthDate: birthDate,
    basicSalary: Math.round(Math.random() * 100).toString(),
    status: status,
    group: group[Math.round(Math.random() * (group.length - 1))],
    description: desc
  };
}


