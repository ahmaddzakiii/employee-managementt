import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';


@Component({
  selector: 'app-view-detail-page',
  standalone: true,
  imports: [FormsModule, CommonModule, MatButtonModule],
  templateUrl: './view-detail-page.component.html',
  styleUrl: './view-detail-page.component.css'
})
export class ViewDetailPageComponent {
  constructor(
    private router: Router,
    public dialogRef: MatDialogRef<ViewDetailPageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onOk() {
    this.dialogRef.close();
  }
}
