import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-add-employee-page',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatNativeDateModule,
    CommonModule,
    MatButtonModule,
  ],
  templateUrl: './add-employee-page.component.html',
  styleUrl: './add-employee-page.component.css',
})
export class AddEmployeePageComponent implements OnInit {
  addEmployeeForm: FormGroup;
  isEmail: boolean | undefined;

  minDate: Date = new Date();
  maxDate: Date = new Date('2024-12-01T05:01:19.8471223Z');

  constructor(private router: Router, private fb: FormBuilder) {
    this.addEmployeeForm = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthDate: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      basicSalary: ['', [Validators.required]],
      group: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    console.log('add-employee-page localStorage -> ', localStorage);
  }

  onSubmit() {
    // var patternOfEmail = new RegExp("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
    var patternOfEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (patternOfEmail.test(this.addEmployeeForm.value.email) == true) {
      this.isEmail = true;
    } else {
      this.isEmail = false;
    }

    console.log('employeeForm -> ', this.addEmployeeForm.value);
    (localStorage.setItem('dataNewEmployee', JSON.stringify(this.addEmployeeForm.value)));
    this.router.navigate(['/home']);
  }

  onCancel() {
    this.router.navigate(['/home']);
  }

  myFilter = (d: Date | null): boolean => {
    const day = d || new Date();
    return day <= this.minDate
  };
}
